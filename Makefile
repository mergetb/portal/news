.PHONY: clean all

all: verify

verify: main.go
	go build -o $@ main.go

clean:
	rm verify
