# news

## Repository for dynamic Merge Testbed News

This repository publishes news about Merge Testbeds to gitlab Pages. It is then
consumed by Merge front-end applications.

When an update is pushed to `main`, the files will be verified to have the correct format and fields, then get published via gitlab pages. The URls to consume will have the format `=https://mergetb.gitlab.io/portal/news/[FILENAME]`. e.g. `https://mergetb.gitlab.io/portal/news/news.json` or `https://mergetb.gitlab.io/portal/news/alerts.sphere.json`

Fields in news items or alerts have the following JSON format:

* `id` - a string that must be unique across other items in the same list.
* `title` - The title of the news item or alert.
* `desc` - the text of the neww item or alert. Can be HTML.
* `timestamp` - the timestamp of the item. 
* `level`: the level of the alert or news item. Must be one of "info", "success", "warning", or "danger". Icons and message color will reflect this value.

The news/alert file is an array of these items. 

Tne `news` file has a top level "title" as well. 

Sample alerts file:
```json
{
    "alerts": [
        {
            "id": "000-test-000",
            "title": "Test Alert",
            "desc": "This is a test of the portal alert system.{' '}<a href='http://isi.edu'>Link about the issue.</a>",
            "timestamp": "2024-11-25T13:00:00Z",
            "level": "danger"
        },
        {
            "id": "001-test-001",
            "title": "Test Warning",
            "desc": "System is unstable. Please save all work.",
            "timestamp": "2024-11-26T13:00:00Z",
            "level": "warning"
        }
    ]
}
```

Sample news file:
```json
{
	"title": "MergeTB News",
	"news": [
		{
			"id": "002-lighthousetb-maintenance",
			"title": "Upcoming Lighthouse Portal Maintenance",
			"desc": "<p>We will be performing maintenance on Wednesday 03/08/2023 at 10AM PST. The maintanance will provide more storage on XDCs and alleviate the recent issues we've seen with XDC instability. The maintenance will likely be quick, but may cause temporary disruption to the portal access and will require reboots of your XDCs.</p>",
			"timestamp": "2022-03-04T00:38:00Z",
			"level": "info"
		},
		{
			"id": "001-materialization-status-is-ready",
			"title": "Materialization Status is Ready!",
			"desc": "<p>Read all about it <a href=\"https://mergetb.org/blog/2022/11/22/materialization-status-is-ready/\">on the blog.</a><br><strong>Note that materializations before this feature was rolled will show \"Error\", as they do not have the metadata needed to support status trees, but they will still be running in whatever state they were before!</strong></p>",
			"timestamp": "2022-11-23T22:00:00Z"
		},
		{
			"id": "000-lighthousetb-status-maintainance",
			"title": "Completed! Scheduled LighthouseTB Maintainance for 2022-11-23",
			"desc": "Edit: This is now complete!<br>There will be maintainance on 2022-11-23 to upgrade the Lighthouse services for the materialization status feature. <br>Although we do not expect any issues, as it is a major upgrade, it is possible that issues could arise.",
			"timestamp": "2022-11-23T22:00:00Z",
			"level": "success"
		}
	]
}
```
