// This verifies that a news.json meets the required format

package main

import (
	"encoding/json"
	"fmt"
	"os"
	"regexp"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"golang.org/x/net/html"
	"k8s.io/apimachinery/pkg/util/sets"
)

var validLevels = sets.NewString("", "default", "info", "success", "warning", "danger")

const validIdExpr = "^[a-z0-9-]+$"

type ItemType string

const (
	NEWS  ItemType = "news"
	ALERT ItemType = "alert"
)

type Item struct {
	Id        string
	Title     string
	Desc      string
	Timestamp string
	Level     string
}

type News struct {
	Title string
	News  []Item
}

type Alerts struct {
	Alerts []Item
}

func main() {
	var root = &cobra.Command{
		Use:   "verify <news.json>",
		Short: "Verify that a JSON file matches the news/alert spec",
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			filename := "news.json"

			if len(args) != 0 {
				filename = args[0]
			}

			err := verify(filename)

			if err == nil {
				log.Infof("%s is ok!", filename)
			} else {
				log.Fatalf("bad file %s: %s", filename, err)
			}
		},
	}

	root.Execute()

}

func verify(filename string) error {
	buf, err := os.ReadFile(filename)
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(strings.NewReader(string(buf)))
	decoder.DisallowUnknownFields()

	var news News
	nerr := decoder.Decode(&news)
	if nerr != nil {

		if nerr.Error() == "json: unknown field \"alerts\"" { // go needs types errors here.
			decoder = json.NewDecoder(strings.NewReader(string(buf)))
			decoder.DisallowUnknownFields()
			var alerts Alerts
			err = decoder.Decode(&alerts)
			if err != nil {
				return fmt.Errorf("Error parsing %s", filename)
			}
			return verifyAlerts(alerts)
		}

		return nerr
	}

	return verifyNews(news)
}

func verifyNews(news News) error {
	if strings.TrimSpace(news.Title) == "" {
		return fmt.Errorf("News \"title\" field is empty!")
	}

	return verifyItems(news.News, NEWS)
}

func verifyAlerts(alerts Alerts) error {

	// No alerts is ok
	if len(alerts.Alerts) == 0 {
		return nil
	}

	return verifyItems(alerts.Alerts, ALERT)
}

func verifyItems(items []Item, which ItemType) error {

	if len(items) == 0 {
		return fmt.Errorf("The %s array contains no items!", which)
	}

	ids := make(sets.String)
	id_regex, err := regexp.Compile(validIdExpr)
	if err != nil {
		return err
	}

	for _, item := range items {

		if strings.TrimSpace(item.Id) == "" {
			return fmt.Errorf("A %s item has an empty id field!", which)
		}

		if strings.TrimSpace(item.Title) == "" {
			return fmt.Errorf("A %s item has an empty title field!", which)
		}

		if strings.TrimSpace(item.Desc) == "" {
			return fmt.Errorf("A %s item has an empty desc field!", which)
		}

		if strings.TrimSpace(item.Timestamp) == "" {
			return fmt.Errorf("A %s item has an empty timestamp field!", which)
		}

		// level can be empty
		// alert expiration can be empty

		if !id_regex.Match([]byte(item.Id)) {
			return fmt.Errorf("The %s item \"%s\" failed to match the regexp: \"%s\"", which, item.Id, validIdExpr)
		}

		if ids.Has(item.Id) {
			return fmt.Errorf("There are multiple %s items with the id: \"%s\"", which, item.Id)
		}

		ids.Insert(item.Id)

		_, err := time.Parse(time.RFC3339, item.Timestamp)
		if err != nil {
			return err
		}

		_, err = html.Parse(strings.NewReader(item.Desc))
		if err != nil {
			return err
		}

		if !validLevels.Has(item.Level) {
			return fmt.Errorf("level: \"%s\" is not one of: [<null> default info success warning danger]", item.Level)
		}
	}

	return nil

}
