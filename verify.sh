#!/bin/bash

set -e

jsons=$(ls news/*.json alerts/*.json)

for j in ${jsons[@]}; do
	./verify $j
done
